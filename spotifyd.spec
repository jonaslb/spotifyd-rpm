Name:       spotifyd
%define gittag 0.2.11
%define version 0.2.11
Version:    %{version}
Release:    1%{?dist}
BuildArch: x86_64
Summary:    Turns your PC into a spotify device for higher quality audio and remote control
License:    GPLv3
Source0:    https://github.com/Spotifyd/spotifyd/archive/%{gittag}.tar.gz
BuildRequires: cargo openssl-devel rust-libdbus-sys-devel alsa-lib-devel pulseaudio-libs-devel

%description
Turns your PC into a spotify device for higher quality audio and remote control.
It is built with support for Pulseaudio and dbus media player control (mpris).
See the readme on https://github.com/Spotifyd/spotifyd for further details.

%global debug_package %{nil}
# Skip debug package, perhaps do it in future

%prep
%autosetup -n spotifyd-%{version}

%build
ls
cargo build --release --features pulseaudio_backend,dbus_mpris

%install
mkdir -p %{buildroot}/%{_bindir}
cp target/release/spotifyd %{buildroot}/%{_bindir}/spotifyd

%clean
rm -rf %{buildroot}

%files
%license LICENCE
%{_bindir}/spotifyd

%changelog
# Not used
